class AddCommunicationSettingsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :calls_enabled, :boolean, default: false, null: false
    add_column :users, :emails_enabled, :boolean, default: true, null: false
  end
end
