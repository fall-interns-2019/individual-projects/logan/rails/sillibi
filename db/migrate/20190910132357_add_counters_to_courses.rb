class AddCountersToCourses < ActiveRecord::Migration[6.0]
  def change
    add_column :courses, :syllabus_count, :integer, default: 0, null: false
    add_column :courses, :assignment_count, :integer, default: 0, null: false
  end
end
