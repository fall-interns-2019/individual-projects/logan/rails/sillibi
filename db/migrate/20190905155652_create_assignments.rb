class CreateAssignments < ActiveRecord::Migration[6.0]
  def change
    create_table :assignments do |t|
      t.string :name
      t.datetime :due_date
      t.string :description
      t.integer :possible_points
      t.references :course, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end
end
