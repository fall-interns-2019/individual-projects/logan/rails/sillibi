class CreateSyllabuses < ActiveRecord::Migration[6.0]
  def change
    create_table :syllabuses do |t|
      t.references :course, null: false, foreign_key: true, index: true
      t.text :image

      t.timestamps
    end
  end
end
