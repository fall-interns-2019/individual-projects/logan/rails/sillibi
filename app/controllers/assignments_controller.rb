class AssignmentsController < ActiveRecordApi::Rest::Controller
  before_action :authenticate_user!

  def index
    @models_full_results = current_user.assignments.limit(100) unless params[:course_id]
    super
  end

  def create
    @model = current_user.send(controller_name).build(modifiable_params)
    super
  end
end