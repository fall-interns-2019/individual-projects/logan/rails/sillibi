class SyllabusesController < ActiveRecordApi::Rest::Controller
  before_action :authenticate_user!

  def index
    @models_full_results = current_user.syllabuses unless params[:course_id]
    super
  end

  def create
    @model = current_user.syllabuses.build(modifiable_params)
    super
  end
end