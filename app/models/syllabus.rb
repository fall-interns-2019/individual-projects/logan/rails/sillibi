class Syllabus < ApplicationRecord
  belongs_to :course
  has_one :user, through: :course

  after_create :increment_counter
  after_destroy :decrement_counter

  private

  def update_counter(inc)
    course.update(syllabus_count: course.syllabus_count + inc)
  end

  def increment_counter
    update_counter(1)
  end

  def decrement_counter
    update_counter(-1)
  end
end
