Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'

  mount_devise_token_auth_for 'Admin', at: 'admin_auth'

  as :admin do
    # Define routes for Admin within this block.
  end

  root to: 'application#index'

  resources_only = %i[index show create update destroy]

  resources :courses, only: resources_only
  resources :assignments, only: resources_only
  resources :syllabuses, only: resources_only
end
